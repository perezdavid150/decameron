toc.dat                                                                                             0000600 0004000 0002000 00000041721 13421706513 0014446 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP           &                 w         	   decameron    11.1    11.1 4    2           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false         3           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false         4           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false         5           1262    16393 	   decameron    DATABASE     �   CREATE DATABASE decameron WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Colombia.1252' LC_CTYPE = 'Spanish_Colombia.1252';
    DROP DATABASE decameron;
             postgres    false         �            1259    16434    Acomodaciones    TABLE     �   CREATE TABLE public."Acomodaciones" (
    "AcomodacionesId" integer NOT NULL,
    "TipoHabitacionId" integer NOT NULL,
    "NombreAcomodacion" character varying(100)
);
 #   DROP TABLE public."Acomodaciones";
       public         postgres    false         �            1259    16430 !   Acomodaciones_AcomodacionesId_seq    SEQUENCE     �   CREATE SEQUENCE public."Acomodaciones_AcomodacionesId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public."Acomodaciones_AcomodacionesId_seq";
       public       postgres    false    202         6           0    0 !   Acomodaciones_AcomodacionesId_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public."Acomodaciones_AcomodacionesId_seq" OWNED BY public."Acomodaciones"."AcomodacionesId";
            public       postgres    false    200         �            1259    16432 "   Acomodaciones_TipoHabitacionId_seq    SEQUENCE     �   CREATE SEQUENCE public."Acomodaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public."Acomodaciones_TipoHabitacionId_seq";
       public       postgres    false    202         7           0    0 "   Acomodaciones_TipoHabitacionId_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public."Acomodaciones_TipoHabitacionId_seq" OWNED BY public."Acomodaciones"."TipoHabitacionId";
            public       postgres    false    201         �            1259    16416    Hoteles    TABLE     �   CREATE TABLE public."Hoteles" (
    "HotelId" integer NOT NULL,
    "NombreHotel" character varying(100),
    "Direccion" character varying(100),
    "NIT" character varying(50),
    "NumeroHabitaciones" bigint,
    "Ciudad" character varying(100)
);
    DROP TABLE public."Hoteles";
       public         postgres    false         �            1259    16452    HotelesTiposHabitaciones    TABLE     �   CREATE TABLE public."HotelesTiposHabitaciones" (
    "HotelTipoHabitacionId" integer NOT NULL,
    "HotelId" integer NOT NULL,
    "TipoHabitacionId" integer NOT NULL,
    "CantidadHabitaciones" integer,
    "AcomodacionId" integer NOT NULL
);
 .   DROP TABLE public."HotelesTiposHabitaciones";
       public         postgres    false         �            1259    16470 *   HotelesTiposHabitaciones_AcomodacionId_seq    SEQUENCE     �   CREATE SEQUENCE public."HotelesTiposHabitaciones_AcomodacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public."HotelesTiposHabitaciones_AcomodacionId_seq";
       public       postgres    false    206         8           0    0 *   HotelesTiposHabitaciones_AcomodacionId_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public."HotelesTiposHabitaciones_AcomodacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."AcomodacionId";
            public       postgres    false    207         �            1259    16448 $   HotelesTiposHabitaciones_HotelId_seq    SEQUENCE     �   CREATE SEQUENCE public."HotelesTiposHabitaciones_HotelId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public."HotelesTiposHabitaciones_HotelId_seq";
       public       postgres    false    206         9           0    0 $   HotelesTiposHabitaciones_HotelId_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public."HotelesTiposHabitaciones_HotelId_seq" OWNED BY public."HotelesTiposHabitaciones"."HotelId";
            public       postgres    false    204         �            1259    16446 2   HotelesTiposHabitaciones_HotelTipoHabitacionId_seq    SEQUENCE     �   CREATE SEQUENCE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq";
       public       postgres    false    206         :           0    0 2   HotelesTiposHabitaciones_HotelTipoHabitacionId_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."HotelTipoHabitacionId";
            public       postgres    false    203         �            1259    16450 -   HotelesTiposHabitaciones_TipoHabitacionId_seq    SEQUENCE     �   CREATE SEQUENCE public."HotelesTiposHabitaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 F   DROP SEQUENCE public."HotelesTiposHabitaciones_TipoHabitacionId_seq";
       public       postgres    false    206         ;           0    0 -   HotelesTiposHabitaciones_TipoHabitacionId_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."HotelesTiposHabitaciones_TipoHabitacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."TipoHabitacionId";
            public       postgres    false    205         �            1259    16414    Hoteles_HotelId_seq    SEQUENCE     �   CREATE SEQUENCE public."Hoteles_HotelId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."Hoteles_HotelId_seq";
       public       postgres    false    197         <           0    0    Hoteles_HotelId_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Hoteles_HotelId_seq" OWNED BY public."Hoteles"."HotelId";
            public       postgres    false    196         �            1259    16424    TiposHabitaciones    TABLE     �   CREATE TABLE public."TiposHabitaciones" (
    "TipoHabitacionId" integer NOT NULL,
    "TipoHabitacion" character varying(100)
);
 '   DROP TABLE public."TiposHabitaciones";
       public         postgres    false         �            1259    16422 &   TiposHabitaciones_TipoHabitacionId_seq    SEQUENCE     �   CREATE SEQUENCE public."TiposHabitaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public."TiposHabitaciones_TipoHabitacionId_seq";
       public       postgres    false    199         =           0    0 &   TiposHabitaciones_TipoHabitacionId_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public."TiposHabitaciones_TipoHabitacionId_seq" OWNED BY public."TiposHabitaciones"."TipoHabitacionId";
            public       postgres    false    198         �
           2604    16437    Acomodaciones AcomodacionesId    DEFAULT     �   ALTER TABLE ONLY public."Acomodaciones" ALTER COLUMN "AcomodacionesId" SET DEFAULT nextval('public."Acomodaciones_AcomodacionesId_seq"'::regclass);
 P   ALTER TABLE public."Acomodaciones" ALTER COLUMN "AcomodacionesId" DROP DEFAULT;
       public       postgres    false    200    202    202         �
           2604    16438    Acomodaciones TipoHabitacionId    DEFAULT     �   ALTER TABLE ONLY public."Acomodaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."Acomodaciones_TipoHabitacionId_seq"'::regclass);
 Q   ALTER TABLE public."Acomodaciones" ALTER COLUMN "TipoHabitacionId" DROP DEFAULT;
       public       postgres    false    202    201    202         �
           2604    16419    Hoteles HotelId    DEFAULT     x   ALTER TABLE ONLY public."Hoteles" ALTER COLUMN "HotelId" SET DEFAULT nextval('public."Hoteles_HotelId_seq"'::regclass);
 B   ALTER TABLE public."Hoteles" ALTER COLUMN "HotelId" DROP DEFAULT;
       public       postgres    false    196    197    197         �
           2604    16455 .   HotelesTiposHabitaciones HotelTipoHabitacionId    DEFAULT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "HotelTipoHabitacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"'::regclass);
 a   ALTER TABLE public."HotelesTiposHabitaciones" ALTER COLUMN "HotelTipoHabitacionId" DROP DEFAULT;
       public       postgres    false    203    206    206         �
           2604    16456     HotelesTiposHabitaciones HotelId    DEFAULT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "HotelId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_HotelId_seq"'::regclass);
 S   ALTER TABLE public."HotelesTiposHabitaciones" ALTER COLUMN "HotelId" DROP DEFAULT;
       public       postgres    false    204    206    206         �
           2604    16457 )   HotelesTiposHabitaciones TipoHabitacionId    DEFAULT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_TipoHabitacionId_seq"'::regclass);
 \   ALTER TABLE public."HotelesTiposHabitaciones" ALTER COLUMN "TipoHabitacionId" DROP DEFAULT;
       public       postgres    false    205    206    206         �
           2604    16472 &   HotelesTiposHabitaciones AcomodacionId    DEFAULT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "AcomodacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_AcomodacionId_seq"'::regclass);
 Y   ALTER TABLE public."HotelesTiposHabitaciones" ALTER COLUMN "AcomodacionId" DROP DEFAULT;
       public       postgres    false    207    206         �
           2604    16427 "   TiposHabitaciones TipoHabitacionId    DEFAULT     �   ALTER TABLE ONLY public."TiposHabitaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."TiposHabitaciones_TipoHabitacionId_seq"'::regclass);
 U   ALTER TABLE public."TiposHabitaciones" ALTER COLUMN "TipoHabitacionId" DROP DEFAULT;
       public       postgres    false    198    199    199         *          0    16434    Acomodaciones 
   TABLE DATA               e   COPY public."Acomodaciones" ("AcomodacionesId", "TipoHabitacionId", "NombreAcomodacion") FROM stdin;
    public       postgres    false    202       2858.dat %          0    16416    Hoteles 
   TABLE DATA               q   COPY public."Hoteles" ("HotelId", "NombreHotel", "Direccion", "NIT", "NumeroHabitaciones", "Ciudad") FROM stdin;
    public       postgres    false    197       2853.dat .          0    16452    HotelesTiposHabitaciones 
   TABLE DATA               �   COPY public."HotelesTiposHabitaciones" ("HotelTipoHabitacionId", "HotelId", "TipoHabitacionId", "CantidadHabitaciones", "AcomodacionId") FROM stdin;
    public       postgres    false    206       2862.dat '          0    16424    TiposHabitaciones 
   TABLE DATA               S   COPY public."TiposHabitaciones" ("TipoHabitacionId", "TipoHabitacion") FROM stdin;
    public       postgres    false    199       2855.dat >           0    0 !   Acomodaciones_AcomodacionesId_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public."Acomodaciones_AcomodacionesId_seq"', 7, true);
            public       postgres    false    200         ?           0    0 "   Acomodaciones_TipoHabitacionId_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public."Acomodaciones_TipoHabitacionId_seq"', 1, false);
            public       postgres    false    201         @           0    0 *   HotelesTiposHabitaciones_AcomodacionId_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_AcomodacionId_seq"', 1, false);
            public       postgres    false    207         A           0    0 $   HotelesTiposHabitaciones_HotelId_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_HotelId_seq"', 1, false);
            public       postgres    false    204         B           0    0 2   HotelesTiposHabitaciones_HotelTipoHabitacionId_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"', 1, false);
            public       postgres    false    203         C           0    0 -   HotelesTiposHabitaciones_TipoHabitacionId_seq    SEQUENCE SET     ^   SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_TipoHabitacionId_seq"', 1, false);
            public       postgres    false    205         D           0    0    Hoteles_HotelId_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."Hoteles_HotelId_seq"', 2, true);
            public       postgres    false    196         E           0    0 &   TiposHabitaciones_TipoHabitacionId_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."TiposHabitaciones_TipoHabitacionId_seq"', 3, true);
            public       postgres    false    198         �
           2606    16440     Acomodaciones Acomodaciones_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY public."Acomodaciones"
    ADD CONSTRAINT "Acomodaciones_pkey" PRIMARY KEY ("AcomodacionesId");
 N   ALTER TABLE ONLY public."Acomodaciones" DROP CONSTRAINT "Acomodaciones_pkey";
       public         postgres    false    202         �
           2606    16459 6   HotelesTiposHabitaciones HotelesTiposHabitaciones_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "HotelesTiposHabitaciones_pkey" PRIMARY KEY ("HotelTipoHabitacionId");
 d   ALTER TABLE ONLY public."HotelesTiposHabitaciones" DROP CONSTRAINT "HotelesTiposHabitaciones_pkey";
       public         postgres    false    206         �
           2606    16421    Hoteles Hoteles_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public."Hoteles"
    ADD CONSTRAINT "Hoteles_pkey" PRIMARY KEY ("HotelId");
 B   ALTER TABLE ONLY public."Hoteles" DROP CONSTRAINT "Hoteles_pkey";
       public         postgres    false    197         �
           2606    16429 (   TiposHabitaciones TiposHabitaciones_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public."TiposHabitaciones"
    ADD CONSTRAINT "TiposHabitaciones_pkey" PRIMARY KEY ("TipoHabitacionId");
 V   ALTER TABLE ONLY public."TiposHabitaciones" DROP CONSTRAINT "TiposHabitaciones_pkey";
       public         postgres    false    199         �
           2606    16441 0   Acomodaciones FK_ACOMODACIONES_TIPOSHABITACIONES    FK CONSTRAINT     �   ALTER TABLE ONLY public."Acomodaciones"
    ADD CONSTRAINT "FK_ACOMODACIONES_TIPOSHABITACIONES" FOREIGN KEY ("TipoHabitacionId") REFERENCES public."TiposHabitaciones"("TipoHabitacionId") ON UPDATE RESTRICT ON DELETE RESTRICT;
 ^   ALTER TABLE ONLY public."Acomodaciones" DROP CONSTRAINT "FK_ACOMODACIONES_TIPOSHABITACIONES";
       public       postgres    false    202    2722    199         �
           2606    16477 B   HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_ACOMODACIONES    FK CONSTRAINT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_ACOMODACIONES" FOREIGN KEY ("AcomodacionId") REFERENCES public."Acomodaciones"("AcomodacionesId");
 p   ALTER TABLE ONLY public."HotelesTiposHabitaciones" DROP CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_ACOMODACIONES";
       public       postgres    false    206    2724    202         �
           2606    16460 <   HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_HOTELES    FK CONSTRAINT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_HOTELES" FOREIGN KEY ("HotelId") REFERENCES public."Hoteles"("HotelId");
 j   ALTER TABLE ONLY public."HotelesTiposHabitaciones" DROP CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_HOTELES";
       public       postgres    false    197    2720    206         �
           2606    16465 F   HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_TIPOSHABITACIONES    FK CONSTRAINT     �   ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_TIPOSHABITACIONES" FOREIGN KEY ("TipoHabitacionId") REFERENCES public."TiposHabitaciones"("TipoHabitacionId");
 t   ALTER TABLE ONLY public."HotelesTiposHabitaciones" DROP CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_TIPOSHABITACIONES";
       public       postgres    false    2722    206    199                                                       2858.dat                                                                                            0000600 0004000 0002000 00000000127 13421706513 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	Sencilla
2	1	Doble
3	2	Triple
4	2	Cuadruple
5	3	Sencilla
6	3	Doble
7	3	Triple
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                         2853.dat                                                                                            0000600 0004000 0002000 00000000077 13421706513 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	aaa	aaa	123	21	aaa
2	Cartagena	Avenida	123	13	Cartagena
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                 2862.dat                                                                                            0000600 0004000 0002000 00000000005 13421706513 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2855.dat                                                                                            0000600 0004000 0002000 00000000041 13421706513 0014252 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Estandar
2	Junior
3	Suite
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               restore.sql                                                                                         0000600 0004000 0002000 00000034132 13421706513 0015371 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE decameron;
--
-- Name: decameron; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE decameron WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Colombia.1252' LC_CTYPE = 'Spanish_Colombia.1252';


ALTER DATABASE decameron OWNER TO postgres;

\connect decameron

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Acomodaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Acomodaciones" (
    "AcomodacionesId" integer NOT NULL,
    "TipoHabitacionId" integer NOT NULL,
    "NombreAcomodacion" character varying(100)
);


ALTER TABLE public."Acomodaciones" OWNER TO postgres;

--
-- Name: Acomodaciones_AcomodacionesId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Acomodaciones_AcomodacionesId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Acomodaciones_AcomodacionesId_seq" OWNER TO postgres;

--
-- Name: Acomodaciones_AcomodacionesId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Acomodaciones_AcomodacionesId_seq" OWNED BY public."Acomodaciones"."AcomodacionesId";


--
-- Name: Acomodaciones_TipoHabitacionId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Acomodaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Acomodaciones_TipoHabitacionId_seq" OWNER TO postgres;

--
-- Name: Acomodaciones_TipoHabitacionId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Acomodaciones_TipoHabitacionId_seq" OWNED BY public."Acomodaciones"."TipoHabitacionId";


--
-- Name: Hoteles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Hoteles" (
    "HotelId" integer NOT NULL,
    "NombreHotel" character varying(100),
    "Direccion" character varying(100),
    "NIT" character varying(50),
    "NumeroHabitaciones" bigint,
    "Ciudad" character varying(100)
);


ALTER TABLE public."Hoteles" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."HotelesTiposHabitaciones" (
    "HotelTipoHabitacionId" integer NOT NULL,
    "HotelId" integer NOT NULL,
    "TipoHabitacionId" integer NOT NULL,
    "CantidadHabitaciones" integer,
    "AcomodacionId" integer NOT NULL
);


ALTER TABLE public."HotelesTiposHabitaciones" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones_AcomodacionId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HotelesTiposHabitaciones_AcomodacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HotelesTiposHabitaciones_AcomodacionId_seq" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones_AcomodacionId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HotelesTiposHabitaciones_AcomodacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."AcomodacionId";


--
-- Name: HotelesTiposHabitaciones_HotelId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HotelesTiposHabitaciones_HotelId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HotelesTiposHabitaciones_HotelId_seq" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones_HotelId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HotelesTiposHabitaciones_HotelId_seq" OWNED BY public."HotelesTiposHabitaciones"."HotelId";


--
-- Name: HotelesTiposHabitaciones_HotelTipoHabitacionId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones_HotelTipoHabitacionId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."HotelTipoHabitacionId";


--
-- Name: HotelesTiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."HotelesTiposHabitaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."HotelesTiposHabitaciones_TipoHabitacionId_seq" OWNER TO postgres;

--
-- Name: HotelesTiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."HotelesTiposHabitaciones_TipoHabitacionId_seq" OWNED BY public."HotelesTiposHabitaciones"."TipoHabitacionId";


--
-- Name: Hoteles_HotelId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Hoteles_HotelId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Hoteles_HotelId_seq" OWNER TO postgres;

--
-- Name: Hoteles_HotelId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Hoteles_HotelId_seq" OWNED BY public."Hoteles"."HotelId";


--
-- Name: TiposHabitaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TiposHabitaciones" (
    "TipoHabitacionId" integer NOT NULL,
    "TipoHabitacion" character varying(100)
);


ALTER TABLE public."TiposHabitaciones" OWNER TO postgres;

--
-- Name: TiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."TiposHabitaciones_TipoHabitacionId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."TiposHabitaciones_TipoHabitacionId_seq" OWNER TO postgres;

--
-- Name: TiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."TiposHabitaciones_TipoHabitacionId_seq" OWNED BY public."TiposHabitaciones"."TipoHabitacionId";


--
-- Name: Acomodaciones AcomodacionesId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Acomodaciones" ALTER COLUMN "AcomodacionesId" SET DEFAULT nextval('public."Acomodaciones_AcomodacionesId_seq"'::regclass);


--
-- Name: Acomodaciones TipoHabitacionId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Acomodaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."Acomodaciones_TipoHabitacionId_seq"'::regclass);


--
-- Name: Hoteles HotelId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Hoteles" ALTER COLUMN "HotelId" SET DEFAULT nextval('public."Hoteles_HotelId_seq"'::regclass);


--
-- Name: HotelesTiposHabitaciones HotelTipoHabitacionId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "HotelTipoHabitacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"'::regclass);


--
-- Name: HotelesTiposHabitaciones HotelId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "HotelId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_HotelId_seq"'::regclass);


--
-- Name: HotelesTiposHabitaciones TipoHabitacionId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_TipoHabitacionId_seq"'::regclass);


--
-- Name: HotelesTiposHabitaciones AcomodacionId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones" ALTER COLUMN "AcomodacionId" SET DEFAULT nextval('public."HotelesTiposHabitaciones_AcomodacionId_seq"'::regclass);


--
-- Name: TiposHabitaciones TipoHabitacionId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TiposHabitaciones" ALTER COLUMN "TipoHabitacionId" SET DEFAULT nextval('public."TiposHabitaciones_TipoHabitacionId_seq"'::regclass);


--
-- Data for Name: Acomodaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Acomodaciones" ("AcomodacionesId", "TipoHabitacionId", "NombreAcomodacion") FROM stdin;
\.
COPY public."Acomodaciones" ("AcomodacionesId", "TipoHabitacionId", "NombreAcomodacion") FROM '$$PATH$$/2858.dat';

--
-- Data for Name: Hoteles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Hoteles" ("HotelId", "NombreHotel", "Direccion", "NIT", "NumeroHabitaciones", "Ciudad") FROM stdin;
\.
COPY public."Hoteles" ("HotelId", "NombreHotel", "Direccion", "NIT", "NumeroHabitaciones", "Ciudad") FROM '$$PATH$$/2853.dat';

--
-- Data for Name: HotelesTiposHabitaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."HotelesTiposHabitaciones" ("HotelTipoHabitacionId", "HotelId", "TipoHabitacionId", "CantidadHabitaciones", "AcomodacionId") FROM stdin;
\.
COPY public."HotelesTiposHabitaciones" ("HotelTipoHabitacionId", "HotelId", "TipoHabitacionId", "CantidadHabitaciones", "AcomodacionId") FROM '$$PATH$$/2862.dat';

--
-- Data for Name: TiposHabitaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."TiposHabitaciones" ("TipoHabitacionId", "TipoHabitacion") FROM stdin;
\.
COPY public."TiposHabitaciones" ("TipoHabitacionId", "TipoHabitacion") FROM '$$PATH$$/2855.dat';

--
-- Name: Acomodaciones_AcomodacionesId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Acomodaciones_AcomodacionesId_seq"', 7, true);


--
-- Name: Acomodaciones_TipoHabitacionId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Acomodaciones_TipoHabitacionId_seq"', 1, false);


--
-- Name: HotelesTiposHabitaciones_AcomodacionId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_AcomodacionId_seq"', 1, false);


--
-- Name: HotelesTiposHabitaciones_HotelId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_HotelId_seq"', 1, false);


--
-- Name: HotelesTiposHabitaciones_HotelTipoHabitacionId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_HotelTipoHabitacionId_seq"', 1, false);


--
-- Name: HotelesTiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."HotelesTiposHabitaciones_TipoHabitacionId_seq"', 1, false);


--
-- Name: Hoteles_HotelId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Hoteles_HotelId_seq"', 2, true);


--
-- Name: TiposHabitaciones_TipoHabitacionId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."TiposHabitaciones_TipoHabitacionId_seq"', 3, true);


--
-- Name: Acomodaciones Acomodaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Acomodaciones"
    ADD CONSTRAINT "Acomodaciones_pkey" PRIMARY KEY ("AcomodacionesId");


--
-- Name: HotelesTiposHabitaciones HotelesTiposHabitaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "HotelesTiposHabitaciones_pkey" PRIMARY KEY ("HotelTipoHabitacionId");


--
-- Name: Hoteles Hoteles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Hoteles"
    ADD CONSTRAINT "Hoteles_pkey" PRIMARY KEY ("HotelId");


--
-- Name: TiposHabitaciones TiposHabitaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TiposHabitaciones"
    ADD CONSTRAINT "TiposHabitaciones_pkey" PRIMARY KEY ("TipoHabitacionId");


--
-- Name: Acomodaciones FK_ACOMODACIONES_TIPOSHABITACIONES; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Acomodaciones"
    ADD CONSTRAINT "FK_ACOMODACIONES_TIPOSHABITACIONES" FOREIGN KEY ("TipoHabitacionId") REFERENCES public."TiposHabitaciones"("TipoHabitacionId") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_ACOMODACIONES; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_ACOMODACIONES" FOREIGN KEY ("AcomodacionId") REFERENCES public."Acomodaciones"("AcomodacionesId");


--
-- Name: HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_HOTELES; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_HOTELES" FOREIGN KEY ("HotelId") REFERENCES public."Hoteles"("HotelId");


--
-- Name: HotelesTiposHabitaciones FK_HOTELESTIPOSHABITACIONES_TIPOSHABITACIONES; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."HotelesTiposHabitaciones"
    ADD CONSTRAINT "FK_HOTELESTIPOSHABITACIONES_TIPOSHABITACIONES" FOREIGN KEY ("TipoHabitacionId") REFERENCES public."TiposHabitaciones"("TipoHabitacionId");


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      