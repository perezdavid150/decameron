<?php

    class HotelesTiposHabitacionesController extends CI_Controller{
        public function __construct() {
            parent::__construct();
            $this->load->helper("url"); 
            $this->load->model("HotelesTiposHabitacionesModel");
            $this->load->library("session");
        }
     
        public function index($HotelId){
            $Hotels["ver"]=$this->HotelesTiposHabitacionesModel->ver($HotelId);
            $Hotels["disponibles"]=$this->HotelesTiposHabitacionesModel->getDisponibles($HotelId);
            $Hotels["habitaciones"]=$this->HotelesTiposHabitacionesModel->getHabitaciones($HotelId);
            $this->load->view("HotelesTiposHabitacionesView",$Hotels);
        }
        
        public function getAcomodaciones($tipo){
            $acomodaciones = $this->HotelesTiposHabitacionesModel->getAcomodacionesByTipo($tipo);
            $ret["html"] = '';
            foreach($acomodaciones as $a){
                $ret["html"] .= '<option value="'.$a->AcomodacionesId.'">'.$a->NombreAcomodacion.'</option>';
            }
            echo json_encode($ret);
        }
        
        
        public function add(){
            $ret["msg"] = '';
            if($this->input->post("TipoHabitacionId")){
                
                if($this->HotelesTiposHabitacionesModel->getDuplicados($this->input->post("TipoHabitacionId"), $this->input->post("AcomodacionId"), $this->input->post("HotelId")) != 0){
                    $ret["msg"] = 'Ya existe un registro con esta habitación y acomodación para este hotel';
                }else{
                    $add=$this->HotelesTiposHabitacionesModel->add(
                        $this->input->post("HotelId"),
                        $this->input->post("TipoHabitacionId"),
                        $this->input->post("AcomodacionId"),
                        $this->input->post("CantidadHabitaciones")
                    );
                    $ret["msg"] = 'Relacion añadida correctamente';
                }
            }
            echo json_encode($ret);
        }
        
        public function update($HotelTipoHabitacionId){
              if($this->input->post("CantidadHabitaciones")){
                    $update=$this->HotelesTiposHabitacionesModel->update(
                            $HotelTipoHabitacionId,
                            "modificar",
                            $this->input->post("CantidadHabitaciones")
                            );
                    if($update){
                        $msg['msg'] = true;
                        echo json_encode ($msg);
                    }else{
                        $msg['msg'] = false;
                        echo json_encode ($msg);
                    }
                }            
        }
        
        public function delete($HotelTipoHabitacionId){
            
              $eliminar=$this->HotelesTiposHabitacionesModel->delete($HotelTipoHabitacionId);
              if($eliminar==true){
				  $msg['msg'] = true;
                  echo json_encode ($msg);
              }else{
				  $msg['msg'] = false;
                  echo json_encode ($msg);
              }
              return true;
        }
        
    }


?>