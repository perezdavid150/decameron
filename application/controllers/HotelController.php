<?php

    class HotelController extends CI_Controller{
        public function __construct() {
            parent::__construct();
            $this->load->helper("url"); 
            $this->load->model("HotelModel");
            $this->load->library("session");
        }
     
        public function index(){
            $Hotels["ver"]=$this->HotelModel->ver();
            $this->load->view("HotelView",$Hotels);
        }
     
        public function add(){
            if($this->input->post("Direccion")){                
                $add=$this->HotelModel->add(
                    $this->input->post("Direccion"),
                    $this->input->post("NombreHotel"),
                    $this->input->post("NIT"),
                    $this->input->post("NumeroHabitaciones"),
                    $this->input->post("Ciudad")
                );
            }
        }
        
        public function update($HotelId){
              if($this->input->post("Direccion")){
                    $update=$this->HotelModel->update(
                            $HotelId,
                            "modificar",
                            $this->input->post("Direccion"),
                            $this->input->post("NombreHotel"),
                            $this->input->post("NIT"),
                            $this->input->post("NumeroHabitaciones"),
                            $this->input->post("Ciudad")
                            );
                    if($update){
                        $msg['msg'] = true;
                        echo json_encode ($msg);
                    }else{
                        $msg['msg'] = false;
                        echo json_encode ($msg);
                    }
                }            
        }

        public function delete($HotelId){
            
              $eliminar=$this->HotelModel->delete($HotelId);
              if($eliminar==true){
				  $msg['msg'] = true;
                  echo json_encode ($msg);
              }else{
				  $msg['msg'] = false;
                  echo json_encode ($msg);
              }
              return true;
        }
        
    }


?>