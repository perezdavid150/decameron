<?php

class HotelModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function ver() {
        $consulta = $this->db->query('SELECT * FROM public."Hoteles" WHERE "Activo" = \'1\' ORDER BY "HotelId" DESC;');
        return $consulta->result();
    }
    
    public function add($Direccion, $NombreHotel, $NIT, $NumeroHabitaciones, $Ciudad) {
        $consulta = $this->db->query('INSERT INTO public."Hoteles" ("Direccion", "NombreHotel", "NIT", "NumeroHabitaciones", "Ciudad") 
                                        VALUES
                                    (
						\''.$Direccion.'\',
						\''.$NombreHotel.'\',
						\''.$NIT.'\',
						\''.$NumeroHabitaciones.'\',
						\''.$Ciudad.'\'
						)');
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function update($HotelId, $modificar = "NULL", $Direccion = "NULL", $NombreHotel = "NULL", $NIT = "NULL", $NumeroHabitaciones = "NULL", $Ciudad = "NULL") {
        if ($modificar == "NULL") {
            $consulta = $this->db->query("SELECT * FROM Hotel WHERE HotelId=$HotelId");
            return $consulta->result();
        } else {
            $consulta = $this->db->query('UPDATE public."Hoteles" SET 
                            "NombreHotel" = \''.$NombreHotel.'\',
                            "Direccion" = \''.$Direccion.'\',
                            "NIT" = \''.$NIT.'\',
                            "NumeroHabitaciones" = \''.$NumeroHabitaciones.'\',
                            "Ciudad" = \''.$Ciudad.'\' WHERE "HotelId" = '.$HotelId);
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function delete($HotelId) {
        $consulta = $this->db->query('UPDATE public."Hoteles" SET "Activo" = \'0\' WHERE "HotelId"='.$HotelId);
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

}

?>