<?php

class HotelesTiposHabitacionesModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function ver($HotelId) {
        $consulta = $this->db->query('SELECT "H"."HotelTipoHabitacionId", "T"."TipoHabitacion", "A"."NombreAcomodacion", "H"."CantidadHabitaciones" 
                                    FROM public."HotelesTiposHabitaciones" as "H"
                                    INNER JOIN "TiposHabitaciones" as "T" ON "T"."TipoHabitacionId" = "H"."TipoHabitacionId"
                                    INNER JOIN "Acomodaciones" as "A" ON "A"."AcomodacionesId" = "H"."AcomodacionId"
                                    WHERE "H"."Activo" = \'1\' AND "H"."HotelId" = '.$HotelId.'
                                    ORDER BY "H"."HotelTipoHabitacionId" DESC');
        return $consulta->result();
    }
    
    public function getDisponibles($HotelId){
        $consulta = $this->db->query('SELECT 
                            ("NumeroHabitaciones" - 
                             (SELECT SUM("CantidadHabitaciones") FROM public."HotelesTiposHabitaciones" WHERE "HotelId" = '.$HotelId.' AND "Activo" = \'1\')) AS resta, "NombreHotel", "HotelId" 
                              FROM 
                            public."Hoteles" WHERE "HotelId" = '.$HotelId);
        return $consulta->result();
    }
    
    public function getHabitaciones($HotelId){
        $consulta = $this->db->query('SELECT * FROM public."TiposHabitaciones"');
        return $consulta->result();
    }
    
    public function getAcomodacionesByTipo($tipo){
        $consulta = $this->db->query('SELECT * FROM public."Acomodaciones" WHERE "TipoHabitacionId" = \''.$tipo.'\'');
        return $consulta->result();
    }
    
    public function getDuplicados($TipoHabitacionId, $AcomodacionId, $HotelId){
        $consulta = $this->db->query('SELECT * FROM public."HotelesTiposHabitaciones" '
                                    . 'WHERE "HotelId" = \''.$HotelId.'\' AND "TipoHabitacionId" = \''.$TipoHabitacionId.'\' AND "AcomodacionId" = \''.$AcomodacionId.'\' AND "Activo" = \'1\'');
        return $consulta->num_rows();
    }

    public function add($HotelId, $TipoHabitacionId, $AcomodacionId, $CantidadHabitaciones) {
        $consulta = $this->db->query('INSERT INTO public."HotelesTiposHabitaciones" ("HotelId", "TipoHabitacionId", "AcomodacionId", "CantidadHabitaciones", "Activo") 
                                        VALUES
                                    (
						\''.$HotelId.'\',
						\''.$TipoHabitacionId.'\',
						\''.$AcomodacionId.'\',
						\''.$CantidadHabitaciones.'\',
                                                \'1\'
						)');
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

    public function update($HotelTipoHabitacionId, $modificar = "NULL", $CantidadHabitaciones = "NULL") {
        if ($modificar == "NULL") {
            $consulta = $this->db->query("SELECT * FROM Hotel WHERE HotelId=$HotelId");
            return $consulta->result();
        } else {
            $consulta = $this->db->query('UPDATE public."HotelesTiposHabitaciones" SET 
                            "CantidadHabitaciones" = \''.$CantidadHabitaciones.'\'
                             WHERE "HotelTipoHabitacionId" = '.$HotelTipoHabitacionId);
            if ($consulta == true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function delete($HotelTipoHabitacionId) {
        $consulta = $this->db->query('UPDATE public."HotelesTiposHabitaciones" SET "Activo" = \'0\' WHERE "HotelTipoHabitacionId"='.$HotelTipoHabitacionId);
        if ($consulta == true) {
            return true;
        } else {
            return false;
        }
    }

}

?>