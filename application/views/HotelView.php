<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Prueba Hotels</title>
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script>
                       
            function update(update){
                    var update = update;
                    var Direccion = $('#Direccion' + update).val();
                    var NombreHotel = $('#NombreHotel' + update).val();
                    var NIT = $('#NIT' + update).val();
                    var NumeroHabitaciones = $('#NumeroHabitaciones' + update).val();
                    var Ciudad = $('#Ciudad' + update).val();
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("HotelController/update") ?>/" + update,
                        data: {
                            update: update,
                            Direccion: Direccion,
                            NombreHotel: NombreHotel,
                            NIT: NIT,
                            NumeroHabitaciones: NumeroHabitaciones,
                            Ciudad: Ciudad},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                alert('Actualizado correctamente');
                            } else {
                                alert('Ocurrio un error al actualizar el hotel');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });

                };
                
                
                function eliminar(eliminar){
                var eliminar = eliminar;
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("HotelController/delete"); ?>/" + eliminar,
                        data: {action: "delete", eliminar: eliminar},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                $('#tr' + eliminar).html('');
                                alert('Hotel eliminado correctamente');
                            } else {
                                alert('Ocurrio un error al eliminar el hotel');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });
                }
            $(document).ready(function () {

                $('label').on("dblclick", function () {
                    $(this).attr("style", "display:none");
                    var dataSet = $(this).attr("data-set");
                    $('#' + dataSet).attr("style", "display:block");
                });



                $('form#formularioAdd').submit(function (e) {
                    var form = $(this);
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url("HotelController/add"); ?>",
                        data: form.serialize(),
                        dataType: "html",
                        success: function () {
                            alert("Hotel añadido correctamente");
                            location.reload(true);
                        },
                        error: function () {
                            alert("Error añadiendo hotel.");
                        }
                    });
                });

            });
        </script>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <h2>Prueba Hoteles</h2>
        <p>&nbsp;</p>

        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Añadir nuevo
        </button>
        <p>&nbsp;</p>

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Nota:</strong> Para editar, de doble click en el elemento a editar. Una vez editado de click en modificar para guardar los cambios.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Direccion</th>
                    <th scope="col">Nombre Hotel</th>
                    <th scope="col">NIT</th>
                    <th scope="col">#Habitaciones</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($ver as $fila) {
                    ?>
                    <tr id="tr<?= $fila->HotelId; ?>">                        
                        <td>
                            <input type="text" id="Direccion<?= $fila->HotelId; ?>" style="display:none" value="<?= $fila->Direccion; ?>" />
                            <label data-set="Direccion<?= $fila->HotelId; ?>"><?= $fila->Direccion; ?></label>
                        </td>                        
                        <td>
                            <input type="text" id="NombreHotel<?= $fila->HotelId; ?>" style="display:none" value="<?= $fila->NombreHotel; ?>" />
                            <label data-set="NombreHotel<?= $fila->HotelId; ?>"><?= $fila->NombreHotel; ?></label>
                        </td>
                        <td>
                            <input type="text" id="NIT<?= $fila->HotelId; ?>" style="display:none" value="<?= $fila->NIT; ?>" />
                            <label data-set="NIT<?= $fila->HotelId; ?>"><?= $fila->NIT; ?></label>
                        </td>
                        <td>
                            <input type="text" id="NumeroHabitaciones<?= $fila->HotelId; ?>" style="display:none" value="<?= $fila->NumeroHabitaciones; ?>" />
                            <label data-set="NumeroHabitaciones<?= $fila->HotelId; ?>"><?= $fila->NumeroHabitaciones; ?></label>
                        </td>
                        <td>
                            <input type="text" id="Ciudad<?= $fila->HotelId; ?>" style="display:none" value="<?= $fila->Ciudad; ?>" />
                            <label data-set="Ciudad<?= $fila->HotelId; ?>"><?= $fila->Ciudad; ?></label>
                        </td>
                        <td>
                            <a href="#" data-id="<?= $fila->HotelId; ?>" id="update" onclick="update(<?= $fila->HotelId; ?>)" class="btn btn-warning">Modificar</a>
                            <a href="#" data-id="<?= $fila->HotelId; ?>" id="eliminar" onclick="eliminar(<?= $fila->HotelId; ?>)" class="btn btn-danger">Eliminar</a> 
                            <a href="<?php echo base_url("HotelesTiposHabitacionesController/index/".$fila->HotelId); ?>" id="relacion" class="btn btn-info">Relacionar habitaciones</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nuevo Hotel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">




                        <form id="formularioAdd" action="#" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Dirección</label>
                                    <input type="text" name="Direccion" class="form-control" id="Direccion" placeholder="Direccion">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputAddress">Nombre Hotel</label>
                                    <input type="text" name="NombreHotel" class="form-control" id="NombreHotel" placeholder="NombreHotel">                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword4">Nit</label>
                                <input type="text" name="NIT" class="form-control" id="NIT" placeholder="NIT">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputCity"># Habitaciones</label>
                                    <input type="text" name="NumeroHabitaciones" class="form-control" id="NumeroHabitaciones" placeholder="NumeroHabitaciones">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputCity">Ciudad</label>
                                    <input type="text" name="Ciudad" class="form-control" id="Ciudad" placeholder="Ciudad">
                                </div>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <input type="submit" id="Add" class="btn btn-primary" value="Guardar" />
                        </form>





                    </div>
                </div>
            </div>
        </div>
    
    </body>
</html>