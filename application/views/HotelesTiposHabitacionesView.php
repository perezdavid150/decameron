<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Prueba Hotels</title>
        <script type="text/javascript" src="<?php echo asset_url();?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap.min.js"></script>
        <script>
                       
            function update(update){
                    var update = update;
                    var CantidadHabitaciones = $('#CantidadHabitaciones' + update).val();                    
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("HotelesTiposHabitacionesController/update") ?>/" + update,
                        data: {
                            update: update,
                            CantidadHabitaciones: CantidadHabitaciones,},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                alert('Actualizado correctamente');
                                location.reload(true);
                            } else {
                                alert('Ocurrio un error al actualizar la relacion');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });

                };
                
                
                function eliminar(eliminar){
                var eliminar = eliminar;
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("HotelesTiposHabitacionesController/delete"); ?>/" + eliminar,
                        data: {action: "delete", eliminar: eliminar},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                alert('Relacion eliminada correctamente');
                                location.reload(true);
                            } else {
                                alert('Ocurrio un error al eliminar la relacion');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });
                }
            $(document).ready(function () {

                $('label').on("dblclick", function () {
                    $(this).attr("style", "display:none");
                    var dataSet = $(this).attr("data-set");
                    $('#' + dataSet).attr("style", "display:block");
                });

                $('select#TipoHabitacionIdSelect').change(function (){
                    var tipo = $(this).val();
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url("HotelesTiposHabitacionesController/getAcomodaciones"); ?>/" + tipo,
                        dataType: "json",
                        success: function (data) {
                            $('#AcomodacionId').removeAttr("readonly");
                            $('#AcomodacionId').html(data.html);                            
                        },
                        error: function () {
                            alert("Error obteniendo datos.");
                        }
                    });
                });

                $('form#formularioAdd').submit(function (e) {
                    var form = $(this);
                    var cantidad = $('#disponibles').val();
                    if(cantidad < $('#CantidadHabitaciones').val()){
                        alert('No se pueden agregar mas habitaciones de las diponibles');
                    }else{
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: "<?= base_url("HotelesTiposHabitacionesController/add"); ?>",
                            data: form.serialize(),
                            dataType: "json",
                            success: function (data) {
                                alert(data.msg);
                                location.reload(true);
                            },
                            error: function () {
                                alert("Error añadiendo hotel.");
                            }
                        });
                    }
                });

            });
        </script>
        <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap.min.css">
    </head>
    <body>
        <h2><?php echo $disponibles[0]->NombreHotel ?></h2>
        <h2><input type="hidden" value="<?php echo $disponibles[0]->resta ?>" id="disponibles" />Habitaciones disponibles: <?php echo $disponibles[0]->resta ?></h2>
        <p>&nbsp;</p>

        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Añadir nuevo
        </button>
        <p>&nbsp;</p>

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Nota:</strong> Para editar, de doble click en el elemento a editar. Una vez editado de click en modificar para guardar los cambios.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Nota:</strong> Solo puede editar cantidades
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Tipo Habitacion</th>
                    <th scope="col">Acomodacion</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($ver as $fila) {
                    ?>
                    <tr id="tr<?= $fila->HotelTipoHabitacionId; ?>">                        
                        <td>
                            <?= $fila->TipoHabitacion; ?>
                        </td>                        
                        <td>
                            <?= $fila->NombreAcomodacion; ?>
                        </td>
                        <td>
                            <input type="text" id="CantidadHabitaciones<?= $fila->HotelTipoHabitacionId; ?>" style="display:none" value="<?= $fila->CantidadHabitaciones; ?>" />
                            <label data-set="CantidadHabitaciones<?= $fila->HotelTipoHabitacionId; ?>"><?= $fila->CantidadHabitaciones; ?></label>
                        </td>
                        <td>
                            <a href="#" data-id="<?= $fila->HotelTipoHabitacionId; ?>" id="update" onclick="update(<?= $fila->HotelTipoHabitacionId; ?>)" class="btn btn-warning">Modificar</a>
                            <a href="#" data-id="<?= $fila->HotelTipoHabitacionId; ?>" id="eliminar" onclick="eliminar(<?= $fila->HotelTipoHabitacionId; ?>)" class="btn btn-danger">Eliminar</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nueva relación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="formularioAdd" action="#" method="post">
                            <div class="form-group">
                                <input type="hidden" name="HotelId" id="HotelId" value="<?php echo $disponibles[0]->HotelId ?>" />
                                <label for="inputPassword4">Tipo Habitacion</label>
                                <select class="form-control" name="TipoHabitacionId" id="TipoHabitacionIdSelect">
                                    <option value="0">Seleccione...</option>
                                    <?php foreach ($habitaciones as $h) { ?>
                                    <option value="<?php echo $h->TipoHabitacionId; ?>"><?php echo $h->TipoHabitacion; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword4">Acomodación</label>
                                <select class="form-control" name="AcomodacionId" id="AcomodacionId" readonly="readonly">
                                    <option>Seleccione Tipo Habitacion...</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword4">Cantidad Habitaciones</label>
                                <input type="text" name="CantidadHabitaciones" class="form-control" id="CantidadHabitaciones" placeholder="Cantidad Habitaciones">
                            </div>                            
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <input type="submit" id="Add" class="btn btn-primary" value="Guardar" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
    </body>
</html>